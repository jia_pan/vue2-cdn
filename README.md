# Vue2 cdn多節點

Vue 僅允許一個根結點，但有些組件有兩個div，就必須外面再包一個div，但有時候這樣css的階層就會亂掉。

Vue3允許根結點為多個，但Vue2無法支援，但還是可以透過一些插件使Vue2 支援。
如 vue-fragment ，我看網路上大多解法是利用此套件，但教學文都必須使用npm安裝，
但在我仔細看了作者的git hub，發現其實有cdn的寫法， 是透過刪除最外面的節點達成，

以下是參考作者，的Demo

*注意:但經由我的測試，此方式僅適合靜態組件，一但使用此方式創建組件，將脫離Vue追蹤，
v-if、v-show等動態渲染將會出現問題。*

[線上demo](https://codepen.io/misooncodepen/pen/rNzVMZB)
```html

<div id="app">
  <a-c :msg='msg' ></a-c>
</div>

<script>
    const Fragment = {
	install(Vue) {
		// install the teleporter
    Vue.directive('fragment', {
      inserted(element) {
        const fragment = document.createDocumentFragment();
      	Array.from(element.childNodes).forEach(child => fragment.appendChild(child));
        element.parentNode.insertBefore(fragment, element);
        element.parentNode.removeChild(element);
      }
    })


    Vue.component('vue-fragment', {
      template: `<div v-fragment><slot /></div>`
    })
  }
}
Vue.use(Fragment);
let aCom = {
  template:`
  <vue-fragment>
    <div>{{msg}}</div>
    <div>節點1</div>
    <div>節點2</div>
    <div>節點3</div>
  </vue-fragment> 
  `,props:{msg:{}},
}
new Vue({
  el: '#app',
  components:{'a-c':aCom},
  data: {
    msg:'可以多節點'
  },
})
    
</script>  



```

